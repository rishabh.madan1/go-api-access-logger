package apiAccessLogger

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/labstack/echo"
	"github.com/spf13/viper"
)

func Hello() string {
	return "Hello, You!"
}

type bodyDumpResponseWriter struct {
	io.Writer
	http.ResponseWriter
}

// APIAccessLogRequest is the struct for API request to log-ingestion service
type APIAccessLogRequest struct {
	DeviceID         string `json:"deviceId"`
	AppVersion       string `json:"appVersion"`
	DeviceType       string `json:"deviceType"`
	RequestMethod    string `json:"requestMethod"`
	Version          string `json:"version"`
	Controller       string `json:"controller"`
	Method           string `json:"method"`
	URL              string `json:"url"`
	Input            string `json:"input"`
	ResponseStatus   string `json:"responseStatus"`
	ResponseLogData  string `json:"responseLogData"`
	StartTimestamp   string `json:"startTimestamp"`
	EndTimestamp     string `json:"endTimestamp"`
	TimeTaken        string `json:"timeTaken"`
	SdkVersion       string `json:"sdkVersion"`
	NetworkBandwidth string `json:"networkBandwidth"`
	ClientID         string `json:"clientId"`
	BobbleSdkVersion string `json:"bobbleSdkVersion"`
}

func (w *bodyDumpResponseWriter) Write(b []byte) (int, error) {
	return w.Writer.Write(b)
}

// Logs middleware sends the api request access logs
func Logs(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		// ignore, if it's an internal request to log-ingestion API `logs/apiAccessLogs`
		// otherwise, it lands into infinite loop
		if strings.Contains(c.Request().URL.String(), "/logs/apiAccessLogs") {
			return
		}

		log := APIAccessLogRequest{}

		// Start Time
		start := time.Now()

		// Request
		reqBody := []byte{}
		if c.Request().Body != nil {
			reqBody, _ = ioutil.ReadAll(c.Request().Body)
		}
		c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(reqBody))

		// Response
		resBody := new(bytes.Buffer)
		mw := io.MultiWriter(c.Response().Writer, resBody)
		writer := &bodyDumpResponseWriter{Writer: mw, ResponseWriter: c.Response().Writer}
		c.Response().Writer = writer

		if err = next(c); err != nil {
			c.Error(err)
		}

		// Stop Time
		stop := time.Now()

		// Response String
		resBodyString := resBody.String()
		if c.Response().Status == http.StatusOK {
			resBodyString = ""
		}

		endpoint := c.Request().URL.String()
		URLParts := strings.Split(endpoint, "/")

		var version string
		var controller string
		var method string

		if len(URLParts) <= 3 {
			// Controller
			controller = URLParts[1]

			// Method
			method = URLParts[2]

		} else {
			// API Version
			version = URLParts[1]

			// Controller
			controller = URLParts[2]

			// Method
			method = strings.Split(strings.Join(URLParts[3:], "/"), "?")[0]
		}

		inputQueries := c.Request().Form
		var reqBodyMap map[string]interface{}
		json.Unmarshal(reqBody, &reqBodyMap)

		type InputMap map[string]interface{}
		inputMap := InputMap{}

		for key, value := range inputQueries {
			inputMap[key] = value[0]
		}
		for key, value := range reqBodyMap {
			inputMap[key] = value
		}
		inputMap["_SERVER"] = map[string]interface{}{
			"deviceIPAddress": c.RealIP(),
		}
		inputString, _ := json.Marshal(inputMap)

		log.DeviceID = c.QueryParam("deviceId")
		log.AppVersion = c.QueryParam("appVersion")
		log.DeviceType = c.QueryParam("deviceType")
		log.RequestMethod = c.Request().Method
		log.Version = version
		log.Controller = controller
		log.Method = method
		log.URL = c.Request().Host + strings.Split(c.Request().RequestURI, "?")[0]
		log.Input = string(inputString)
		log.ResponseStatus = strconv.Itoa(c.Response().Status)
		log.ResponseLogData = resBodyString
		log.StartTimestamp = fmt.Sprintf("%d-%02d-%02d %02d:%02d:%02d",
			start.Year(), start.Month(), start.Day(),
			start.Hour(), start.Minute(), start.Second())
		log.EndTimestamp = fmt.Sprintf("%d-%02d-%02d %02d:%02d:%02d",
			stop.Year(), stop.Month(), stop.Day(),
			stop.Hour(), stop.Minute(), stop.Second())
		log.TimeTaken = fmt.Sprintf("%.15f", (stop.Sub(start)).Seconds())
		log.SdkVersion = c.QueryParam("sdkVersion")
		log.NetworkBandwidth = c.QueryParam("networkBandwidth")
		log.ClientID = c.QueryParam("clientId")
		log.BobbleSdkVersion = c.QueryParam("bobbleSdkVersion")

		go storeAPIAccessLog(log)

		return
	}
}

// storeAPIAccessLog sends the log to external service (using HTTP call)
func storeAPIAccessLog(log APIAccessLogRequest) (err error) {

	route := fmt.Sprintf("%s/v4/logs/apiAccessLogs", viper.GetString("LOG_INGESTION_API_BASE_URL"))

	body, _ := json.Marshal(log)
	reqBody := bytes.NewBuffer(body)

	response, err := http.Post(route, "application/json", reqBody)

	if err != nil {
		fmt.Println("ERROR: storeAPIAccessLog: err:", err.Error())
	} else if response != nil && response.StatusCode >= 400 {
		data, e := ioutil.ReadAll(response.Body)
		defer response.Body.Close()
		if e != nil {
			return
		}
		fmt.Println("ERROR: storeAPIAccessLog: response:", string(data))
	}

	return
}
